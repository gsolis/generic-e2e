import React from 'react';
import ReactDOM from 'react-dom';
import Prototypes from './prototypes/prototypes';
import App from './components/app/app';
import { Provider  } from 'react-redux';
//import CssReview from './components/css-review';
//import IncDec from './components/increment-decrement';
import configureStore from './store/configure-store';
import './lib/font-awesome/scss/font-awesome.scss';

const store = configureStore();

ReactDOM.render(
    <Provider store={store} >
        <App />
    </Provider>,
    document.querySelector('#main-app')
);






