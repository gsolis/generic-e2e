
import {
    NOTES_LOAD,
    NOTES_LOAD_SUCCESS,
    NOTES_LOAD_FAILURE,
    SET_SHOW_NOTE_DETAILS_DRAWER,
    SELECT_NOTE,
    SELECT_NOTE_SUCCESS,
    SELECT_NOTE_FAILURE,
    SET_EDIT_MODE,
    UPDATE_NOTE_SUCCESS,
    SET_SHOW_NEW_NOTE_DRAWER,
    SAVE_NOTE,
    SAVE_NOTE_SUCCESS,
    SAVE_NOTE_FAILURE,
    NOTE_FORM_CHANGED,
    SET_SEARCH_BY,
    SET_FILTER_BY,
    SET_SHOW_CONFIRM_MODAL,
    DELETE_NOTE,
    DELETE_NOTE_SUCCESS,
    DELETE_NOTE_FAILURE
} from '../actions/types';

/*import {
    NOTES_LOAD,
    NOTES_LOAD_SUCCESS,
    NOTES_LOAD_FAILURE,

    SET_SHOW_NOTE_DETAILS_DRAWER,

    SELECT_NOTE,
    SELECT_NOTE_SUCCESS,

    SAVE_NOTE,
    SAVE_NOTE_SUCCESS,
    UPDATE_NOTE_SUCCESS,
    SAVE_NOTE_FAILURE,

    SET_SHOW_NEW_NOTE_DRAWER,
    SET_SHOW_CONFIRM_MODAL,
    SET_SEARCH_BY,
    SET_FILTER_BY,
    NOTE_FORM_CHANGED,
    SET_EDIT_MODE,

    DELETE_NOTE,
    DELETE_NOTE_SUCCESS
} from '../actions/types';*/

const initialNote = {
        title: '',
        description: '',
        status: 'pending'
    },
    INITIAL_STATE = {
    notes: [],
    newNote: initialNote,
    currentNoteEditing: null,
    showNewNoteDrawer: false,
    showNoteDetailsDrawer: false,
    showDeleteConfirmModal: false,
    selectedNote: null,
    searchInput: '',
    filterByStatus: '',
    isEditMode: false
};

export default function NotesListReducer(state = INITIAL_STATE, action) {
    let newState;
    switch (action.type) {
        case NOTES_LOAD:
            newState = { ...state };
            break;
        case NOTES_LOAD_SUCCESS:
            newState = { ...state, notes: action.payload };
            break;
        case NOTES_LOAD_FAILURE:
            newState = { ...state };
            break;



        case SELECT_NOTE:
            newState = { ...state };
        case SELECT_NOTE_SUCCESS:
            newState = { ...state, showNoteDetailsDrawer: true, selectedNote: action.payload };
            break;



        case SAVE_NOTE:
            newState = { ...state };
            break;
        case SAVE_NOTE_SUCCESS:
            newState = {
                ...state,
                notes: [action.payload, ...state.notes],
                newNote: initialNote,
                currentNoteEditing: null
            };
            break;

            
        case UPDATE_NOTE_SUCCESS:
            const index = state.notes.findIndex(note => note._id === action.payload._id);
            let newNotes = state.notes.slice(0, index);
            newNotes = newNotes.concat(action.payload);
            newNotes = newNotes.concat(state.notes.slice(index + 1));
            newState = { ...state, notes: newNotes, currentNoteEditing: null };
            break;
        case SAVE_NOTE_FAILURE:
            newState = { ...state };
            break;
        case SET_SHOW_NEW_NOTE_DRAWER:
            newState = { ...state, showNewNoteDrawer: action.payload };
            break;
        case SET_SHOW_NOTE_DETAILS_DRAWER:
            newState = {
                ...state,
                showNoteDetailsDrawer: action.payload.show,
                selectedNote: action.payload.note
            };
            break;
        case SET_SHOW_CONFIRM_MODAL:
            newState = {
                ...state,
                showDeleteConfirmModal: action.payload.show,
                selectedNote: action.payload.note
            };
            break;
        case SET_SEARCH_BY:
            newState = {
                ...state,
                searchInput: action.payload
            };
            break;
        case SET_FILTER_BY:
            newState = {
                ...state,
                filterByStatus: action.payload
            };
            break;
        case NOTE_FORM_CHANGED:
            newState = {
                ...state,
                [state.isEditMode ? 'currentNoteEditing' : 'newNote']: action.payload
            };
            break;
        case SET_EDIT_MODE:
            newState = {...state, isEditMode: action.payload };
            break;
        case DELETE_NOTE:
            newState = { ...state };
            break;
        case DELETE_NOTE_SUCCESS:
            newState = {
                ...state,
                notes: state.notes.filter(note => note._id !== action.payload._id),
                showDeleteConfirmModal: false,
                selectedNote: null
            };
            break;
        default:
            newState = state;
    }
    return newState;
}

