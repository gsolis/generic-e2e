import { combineReducers } from 'redux';
import notesList from './notes-list';
import users from './users';

export default combineReducers({
    users,
    notesList
});