import React from 'react';
import NotesListHeader from './notes-list-header';
import NotesListItem from './notes-list-item';
import Drawer from '../common/drawer/drawer';
import Modal from '../common/modal/modal';
import NoteForm from '../note-form/note-form';
import NoteDetails from '../note-details/note-details';
import NotesService from '../../services/notes-service';
import './notes-list.scss';

class NotesList extends React.Component {

    constructor() {
        super();

        this.state = {
            notes: [],
            newNote: {
                title: '',
                description: '',
                status: 'pending'
            },
            searchBy: null,
            filterByStatus: null,
            showDrawer: false,

            selectedNote: null,
            showNoteDetail: false,

            showMergeDrawer: false,
            relativeTitle: null,

            clearFields: false,

            isEditMode: false,
            editingNote: null,

            isRemovedItem: false,

            showDrawerPopUp: false,
            isRemoving: false,
            noteToRemove: null,
            noteSaved: false
        };
    }

    componentDidMount() {
        NotesService.getNotes()
            .then((notes) => {
                this.setState({
                    notes: notes
                });
            })
            .catch(() => {

            });
    }

    handleOpenDrawer = () => {
        this.setState({
            showDrawer: true
        });
    }

    handleDrawerClose = () => {
        this.setState({
            showDrawer: false
        });
    }

    handleNoteSave = () => {
        if (this.isValidNote(this.state.newNote)) {
            NotesService.saveNote(this.state.newNote)
                .then((noteReceived) => {
                    this.setState({
                        notes: [noteReceived, ...this.state.notes],
                        newNote: {
                            title: '',
                            description: '',
                            status: 'pending'
                        }
                    });
                });
        }
    }

    handleFilterByStatus = (status) => {
        this.setState({
            filterByStatus: status
        });
    }

    handleFilterByTitle = (search) => {
        this.setState({
            searchBy: search,
        });
    }

    filterNotes = (item) => {
        const regex = new RegExp((this.state.searchBy || '').toLowerCase(), "g");

        //If no searchBy && no filterByStatus => include all
        if (!this.state.filterByStatus && !this.state.searchBy) {
            return true;
        }

        //If no searchBy && no filterByStatus => include all
        if (this.state.filterByStatus && this.state.searchBy) {
            return item.status === this.state.filterByStatus &&
                regex.test(item.title.toLowerCase());
        }

        if (this.state.filterByStatus) {
            return item.status === this.state.filterByStatus;
        }

        if (this.state.searchBy) {
            return regex.test(item.title.toLowerCase());
        }
    }

    handleNoteSelected = (note) => {
        NotesService.getNoteById(note._id)
            .then((noteReceived) => {
                this.setState({
                    selectedNote: noteReceived,
                    showNoteDetail: true
                });
            })
            .catch(error => {
                this.setState({
                    error: error
                });
            });
    }

    handleCloseNoteDetail = () => {
        this.setState({
            selectedNote: null,
            editingNote: null,
            showNoteDetail: false,
            isEditMode: false
        });
    }

    isValidNote = (note) => {
        var isValid = note ? true : false;
        if (note && note.title.trim() === '' ||
            note && note.description.trim() === '' ||
            note && note.status.trim() === '') {
            isValid = false;
        }
        return isValid;
    }

    handleNoteFormChange = (formData) => {
        if (this.state.isEditMode) {
            this.setState({
                editingNote: formData
            });
        } else {
            this.setState({
                newNote: formData
            });
        }
    }

    handleEditSave = () => {
        if (!this.state.isEditMode) {
            this.setState({
                isEditMode: true
            });
        } else {
            NotesService.updateNote(this.state.editingNote)
                .then(updatedNote => {
                    const index = this.state.notes.findIndex(note => note._id === updatedNote._id);
                    let newNotes = this.state.notes.slice(0, index);
                    newNotes = newNotes.concat(updatedNote);
                    newNotes = newNotes.concat(this.state.notes.slice(index + 1));

                    this.setState({
                        notes: newNotes
                    });
                    this.handleCloseNoteDetail();
                });   
        }
    }
  
    handleRemoveNote = (item) => {
        this.setState({
            isRemoving: true,
            showDrawerPopUp: true,
            noteToRemove: item
        });
    }

    handleModalClose = () => {
        this.setState({
            showDrawerPopUp: false
        });
    }

    handleModalOk = () => {
        if (this.state.noteToRemove) {
            NotesService.deleteNoteById(this.state.noteToRemove._id)
                .then(() => {
                    const notes = this.state.notes.filter(note => note._id !== this.state.noteToRemove._id)
                    this.setState({
                        notes: notes,
                        showDrawerPopUp: false
                    });
                })
                .catch(error => {

                });
        }
    }

    render() {    
        const okBtn = {
                text: 'Create',
                disabled: !this.isValidNote(this.state.newNote),
                callback: this.handleNoteSave
            },
            cancelBtn = {
                text: 'Cancel',
                disabled: false,
                callback: this.handleDrawerClose
            },
            editBtn = {
                text: this.state.isEditMode ? 'Save' : 'Edit',
                disabled: !this.state.isEditMode ? false : !this.isValidNote(this.state.editingNote),
                callback: this.handleEditSave
            },
            cancelBtnDetail = {
                text: 'Cancel',
                disabled: false,
                callback: this.handleCloseNoteDetail
            },
            modalOkBtn = {
                text: 'Ok',
                callback: this.handleModalOk
            },
            modalCancelBtn = {
                text: 'Cancel',
                callback: this.handleModalClose
            };


        return (
            <div className="notes-list">
                <NotesListHeader
                    onOpenDrawer={this.handleOpenDrawer}
                    onSearchChange={this.handleFilterByTitle} 
                    onFilterBySChange={this.handleFilterByStatus} />

                <div className="notes-list-container">
                    {this.state.notes
                        .filter(this.filterNotes)
                        .map((item, i) =>
                            <NotesListItem
                                key={i}
                                item={item}
                                onSelect={this.handleNoteSelected}
                                onRemoveNote={this.handleRemoveNote} />)}
                </div>

                <Drawer
                    show={this.state.showDrawer}
                    title="New note"
                    onClose={this.handleDrawerClose}
                    okBtn={okBtn}
                    cancelBtn={cancelBtn}>
                    <NoteForm
                        onChange={this.handleNoteFormChange}
                        note={this.state.newNote} />
                </Drawer>

                <Drawer
                    show={this.state.showNoteDetail}
                    title={this.state.isEditMode ? `Editing ${this.state.selectedNote.title}` : 'Note details'}
                    onClose={this.handleCloseNoteDetail}
                    okBtn={editBtn}
                    cancelBtn={cancelBtnDetail}>
                    <NoteDetails 
                        note={this.state.editingNote || this.state.selectedNote}
                        editMode={this.state.isEditMode}
                        onChange={this.handleNoteFormChange} />
                </Drawer>

                {this.state.isRemoving &&
                    <Modal
                        show={this.state.showDrawerPopUp}
                        title="Deleting Note"
                        message={`Are you sure to delete note: ${this.state.noteToRemove.title} ?`}
                        okBtn={modalOkBtn}
                        cancelBtn={modalCancelBtn}
                        onClose={this.handleModalClose}>
                    </Modal>}

            </div>
        );
    }

}

export default NotesList;