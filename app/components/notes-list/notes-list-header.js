import React, { PropTypes } from 'react';

class NotesListHeader extends React.Component {

    handleChange = (e) => {
        e.target.name === 'search' &&
            this.props.onSearch(e.target.value);

        e.target.name === 'status' &&
            this.props.onFilterBy(e.target.value);
    }

    render() {
        const { filterBy, search, onNewNoteBtnClick } = this.props;

        return (  
            <div className="notes-list-header">
                <button
                    id="newnote-button"
                    onClick={onNewNoteBtnClick}>
                    <i className="fa fa-plus" aria-hidden="true" id="plus-icon"></i>New
                </button>
                <div className="notes-options-right">
                    <span>Filter by</span>
                    <select
                        name="status"
                        value={filterBy}
                        onChange={this.handleChange}>
                        <option value="">--Select a status--</option>
                        <option value="pending">Pending</option>
                        <option value="done">Done</option>
                    </select>
                    <input
                        type="text"
                        name="search"
                        value={search}
                        placeholder="Search notes..."
                        onChange={this.handleChange} />   
                </div>                
            </div>    
        );
    }

}

 NotesListHeader.propTypes = {
    filterBy: PropTypes.string,
    search: PropTypes.string,
    onNewNoteBtnClick: PropTypes.func,
    onSearch: PropTypes.func,
    onFilterBy: PropTypes.func
 };

export default NotesListHeader;