
import React from 'react';
import moment from 'moment';

class NotesListItem extends React.Component {

    handleSelect = () => {
        this.props.onSelect &&
            this.props.onSelect(this.props.note);
    }

    handleRemoveNote = (e) => {
        e.stopPropagation();
        this.props.onRemoveNote &&
            this.props.onRemoveNote(this.props.note);
    }

    render() {
        const { note } = this.props;
        
        return (
           <div className="note-list-item" onClick={this.handleSelect}>
                <i onClick={this.handleRemoveNote} className="fa fa-times" aria-hidden="true"></i>
                <p>
                    <label className="title">{note.title.toCapital()}</label>
                    <span className="date">{moment(note.date).fromNow()}</span>
                </p>
                <div>
                    <label className="description">{note.description.toCapital()}</label>
                    <span className={`status ${note.status}`}>
                        <i className="fa fa-clock-o" aria-hidden="true"></i>
                        {note.status}
                    </span>
                </div>
            </div>
        );
    }

}

NotesListItem.propTypes = {
    note: React.PropTypes.object,
    onSelect: React.PropTypes.func,
    onRemoveNote: React.PropTypes.func
};

export default NotesListItem;



  
