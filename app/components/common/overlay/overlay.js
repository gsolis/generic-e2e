import React, { PropTypes } from 'react';
import './overlay.scss';

const Overlay = ({ show, children }) => {
    return (
        <div className={`overlay ${show ? 'show' : ''}`}>
            {children}
        </div>
    );
};

Overlay.propTypes = {
    show: PropTypes.bool,
    children:PropTypes.any
};

export default Overlay;