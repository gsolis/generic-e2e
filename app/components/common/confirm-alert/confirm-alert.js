import React, { PropTypes } from 'react';
import './confirm-alert.scss';
import Overlay from '../overlay/overlay';


class ConfirmAlert extends React.Component {

    ok = () => {
        this.props.onClose && this.props.onClose(true);
    };
          
    cancel = () => {
        this.props.onClose && this.props.onClose(false);
    };   

    render() {

        const { show, title, message, onClose, okBtnText, cancelBtnText } = this.props;

        return(
            <Overlay show={show}>
            <div className="confirm-alert">
                <header>
                    <div>
                        {title}
                        <i
                            onClick={this.cancel}
                            className="fa fa-times"
                            aria-hidden="true" />
                    </div>
                </header>
                <section>
                    <p>{message}</p> 
                </section>
                <footer>
                    <button
                        className="cancel-btn"
                        onClick={this.cancel}>
                        {cancelBtnText || 'Cancel'}
                    </button>
                    <button
                        className="ok-btn"
                        onClick={this.ok}>
                        {okBtnText || 'Ok'}
                    </button>
                </footer>
            </div>
        </Overlay>
        );
    }

}







/*
const ConfirmAlert = ({ show, title, message, onClose, okBtnText, cancelBtnText }) => {

    const ok = () => {
            this.props.onClose && this.props.onClose(true);
        },
        cancel = () => {
            this.props.onClose && this.props.onClose(false);
        };        

    return (
        <Overlay show={show}>
            <div className="confirm-alert">
                <header>
                    <div>
                        {title}
                        <i
                            onClick={cancel}
                            className="fa fa-times"
                            aria-hidden="true" />
                    </div>
                </header>
                <section>
                    <p>{message}</p> 
                </section>
                <footer>
                    <button
                        className="cancel-btn"
                        onClick={cancel}>
                        {cancelBtnText || 'Cancel'}
                    </button>
                    <button
                        className="ok-btn"
                        onClick={ok.bind(this)}>
                        {okBtnText || 'Ok'}
                    </button>
                </footer>
            </div>
        </Overlay>
    );

}*/

ConfirmAlert.propTypes = {
   show: PropTypes.bool,
   title: PropTypes.string,
   message: PropTypes.string,
   onClose: PropTypes.func,
   okBtnText: PropTypes.string,
   cancelBtnText: PropTypes.string
};

export default ConfirmAlert;