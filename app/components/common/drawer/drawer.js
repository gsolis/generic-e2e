import React, { PropTypes } from 'react';
import './drawer.scss';
import Overlay from '../overlay/overlay';

class Drawer extends React.Component {

    handleClose = () => {
        this.props.onClose();
    }

    render() {
        const {
            show,
            title,
            okBtn,
            cancelBtn,
            children
        } = this.props;

        return (
            <Overlay show={show}>
                <div className="drawer">
                    <header>
                        <label>{title}</label>
                        <i id="drawer-icon"
                            className="fa fa-close"
                            onClick={this.handleClose}></i>
                    </header>

                    <section>
                        {children}
                    </section>

                    <footer>
                        {cancelBtn &&
                            <button
                                className="close-btn"
                                disabled={cancelBtn.disabled}
                                onClick={cancelBtn.callback}>
                                {cancelBtn.text}
                            </button>}

                         {/*noteDetailsCancelBtn &&
                            <button
                                className="close-btn"
                                disabled={noteDetailsCancelBtn.disabled}
                                onClick={noteDetailsCancelBtn.callback}>
                                {noteDetailsCancelBtn.text}
                            </button>*/}
                        
                        {okBtn &&
                            <button
                                className={`ok-btn ${okBtn.disabled ? 'disabled' : ''}`}
                                disabled={okBtn.disabled}
                                onClick={okBtn.callback}>
                                {okBtn.text}
                            </button>}

                        {/*noteDetailsEditBtn &&
                            <button
                                className={`ok-btn ${noteDetailsEditBtn.disabled ? 'disabled' : ''}`}
                                disabled={noteDetailsEditBtn.disabled}
                                onClick={noteDetailsEditBtn.callback}>
                                {noteDetailsEditBtn.text}
                            </button>*/}
                    </footer>
                </div> 
            </Overlay>
        );
    }

}

Drawer.propTypes = {
    show: PropTypes.bool,
    onClose: PropTypes.func,
    title: PropTypes.string,
    okBtn: PropTypes.object,
    cancelBtn: PropTypes.object,
    children: PropTypes.any,
    editingTitle: PropTypes.string
};

export default Drawer;