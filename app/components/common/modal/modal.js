import React, { PropTypes } from 'react';
import './modal.scss';
import Overlay from '../overlay/overlay';

class Modal extends React.Component {

    close = () => {
        this.props.onClose && this.props.onClose();
    }

    render() {

        const {
            show,
            title,
            message,
            cancelBtn,
            okBtn,
            onClose
        } = this.props;
        
        return (
            <Overlay show={show}>
                <div className="modal">
                    <header>
                        <div>{title}
                        <i onClick={this.close} className="fa fa-times" aria-hidden="true"></i></div>
                    </header>
                    <section>
                        <p>{message}</p> 
                    </section>
                    <footer>
                        <button
                            onClick={cancelBtn.callback}>
                            {cancelBtn.text}
                        </button>
                        <button
                            className="okRemoving"
                            onClick={okBtn.callback}>
                            {okBtn.text}
                        </button>
                    </footer>
                </div>
            </Overlay>
        );
    }

}

Modal.propTypes = {
   show: PropTypes.bool,
   title: PropTypes.string,
   message: PropTypes.string,
   cancelBtn: PropTypes.object,
   okBtn: PropTypes.object,
   onClose: PropTypes.func
};

export default Modal;