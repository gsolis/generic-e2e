import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import './app.scss';
import AppHeader from './app-header';
import AppContent from './app-content';

class App extends React.Component {
    
    render() {
        return (
            <div className="app">
                <AppHeader />
                <AppContent {...this.props} />
            </div>
        );
    }

}

export default App;
