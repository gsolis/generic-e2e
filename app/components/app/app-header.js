import React from 'react';

class AppHeader extends React.Component {

    render() {
        return (
            <header className="app-header">
                <i className="fa fa-bars" aria-hidden="true"></i>
                <h1>Notes manager</h1>
            </header>
        );
    }

}

export default AppHeader;