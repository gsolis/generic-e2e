import React from 'react';
//import NotesList from '../notes-list/notes-list';
import NotesListContainer from '../../containers/notes-list-container';

class AppContent extends React.Component {

    render() {
        return (
            <section className="app-content">
                <NotesListContainer {...this.props} />
            </section>
        );
    }

}

export default AppContent;