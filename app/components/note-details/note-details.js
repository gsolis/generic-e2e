import React, { PropTypes } from 'react';
import NoteForm from '../note-form/note-form';
import moment from 'moment';
import './note-details.scss';

class NotesDetails extends React.Component {
    
    render() {
        const { note } = this.props;

        return(
            <section className="note-details">
                <p>{note && note.title}</p>
                <p className="description">{note && note.description}</p>
                <p className="date"><i className="fa fa-calendar" aria-hidden="true"></i> {note && moment(note.date).fromNow()}</p>
                <p className={`status ${note && note.status}`}><i className="fa fa-clock-o" aria-hidden="true"></i> {note && note.status}</p>
            </section>
        );
    }
}

NotesDetails.PropTypes = {
    note: PropTypes.object
};

export default NotesDetails;