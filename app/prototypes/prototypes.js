
String.prototype.toCapital = function() {
	var chatInUppercase = this[0];
	chatInUppercase = chatInUppercase.toUpperCase();
	var restOfString =  this.substr(1, this.length - 1);
	return `${chatInUppercase}${restOfString}`;
};