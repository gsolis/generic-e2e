const notes = [
    { id: 1, title: 'My 1st note', description: 'Some desc 1', status: 'pending', date: new Date() }, 
    { id: 2, title: 'My 2nd note', description: 'Some desc 2', status: 'done', date: new Date() }, 
    { id: 3, title: 'My 3rd note', description: 'Some desc 3', status: 'pending', date: new Date() }, 
    { id: 4, title: 'My 4th note', description: 'Some desc 4', status: 'done', date: new Date() }, 
    { id: 5, title: 'My 5th note', description: 'Some desc 5', status: 'pending', date: new Date() } 
];
// ARQUITECTURA CLIENTE - SERVIDOR
// HTTP -> [GET, POST, PUT, DELETE]
// CLIENT => REQUEST
// SERVER => RESPONSE
// REST => REPRESENTATIONAL STATE TRANSFER
//RESOURCE => (NOTE MODEL): (SERVER SIDE)
// GET => /notes
// GET => /notes/:id
// POST => /notes
// PUT => /notes/:id
// DELETE => /notes/:id
// CRUD => CREATE, READ, UPDATE, DELETE
const NotesService = {
    //READ => [GET]
    getNotes: () => {
        return fetch('http://localhost:3000/notes', {
                mode: 'cors'
            })
            .then(response => {
                return response.json();
            });
    },
    //READ => [GET]
    getNoteById: (noteId) => {
        return fetch(`http://localhost:3000/notes/${noteId}`)
            .then(response => {
                return response.json();
            });
    },
    //CREATE => [POST]
    saveNote: (note) => {
        const saveUrl = note._id ? `http://localhost:3000/notes/${note._id}` : 'http://localhost:3000/notes';
        return fetch(saveUrl, {
            method: note._id ? 'PUT' : 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(note)
        })
            .then(response => {
                return response.json();
            });
    },
    //UPDATE => [PUT]
    /*updateNote: (noteUpdated) => {
        return fetch(`http://localhost:3000/notes/${noteUpdated._id}`, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(noteUpdated)
        })
            .then(response => {
                return response.json();
            });
    },*/
    //DELETE => [DELETE]
    deleteNoteById: (noteId) => {
        return fetch(`http://localhost:3000/notes/${noteId}`, {
            method: 'DELETE'
        });
    }
};

export default NotesService;