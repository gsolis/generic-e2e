import NotesService from '../services/notes-service';
import {
    NOTES_LOAD,
    NOTES_LOAD_SUCCESS,
    NOTES_LOAD_FAILURE,
    SET_SHOW_NOTE_DETAILS_DRAWER,
    SELECT_NOTE,
    SELECT_NOTE_SUCCESS,
    SELECT_NOTE_FAILURE,
    SET_EDIT_MODE,
    UPDATE_NOTE_SUCCESS,
    SET_SHOW_NEW_NOTE_DRAWER,
    SAVE_NOTE,
    SAVE_NOTE_SUCCESS,
    SAVE_NOTE_FAILURE,
    NOTE_FORM_CHANGED,
    SET_SEARCH_BY,
    SET_FILTER_BY,
    SET_SHOW_CONFIRM_MODAL,
    DELETE_NOTE,
    DELETE_NOTE_SUCCESS,
    DELETE_NOTE_FAILURE
} from './types';

//Loading notes
export const loadNotes = () => {
    return (dispatch) => {
        dispatch({ type: NOTES_LOAD });
        return NotesService.getNotes()
            .then((notes) => {
                dispatch({ type: NOTES_LOAD_SUCCESS, payload: notes });
            })
            .catch((error) => {
                dispatch({ type: NOTES_LOAD_FAILURE, payload: error });
            });
    };
};

//Opening section for the creation of a new note
export const setShowNewNoteDrawer = (value) => {
    return {
        type: SET_SHOW_NEW_NOTE_DRAWER,
        payload: value
    };
};

//Saving note(previously created or new)
export const saveNote = (note) => {
    return (dispatch) => {
        dispatch({ type: SAVE_NOTE });
        return NotesService.saveNote(note)
            .then((noteSaved) => {
                dispatch({
                    type: note._id ? UPDATE_NOTE_SUCCESS : SAVE_NOTE_SUCCESS,
                    payload: noteSaved
                });
            })
            .catch((error) => {
                dispatch({
                    type: SAVE_NOTE_FAILURE,
                    payload: error
                });
            });
    };
};

//Filering existing notes
export const setFilterBy = (status) => {
    return {
        type: SET_FILTER_BY,
        payload: status
    };
};

//Searching for notes
export const setSearchBy = (inputSearch) => {
    return {
        type: SET_SEARCH_BY,
        payload: inputSearch
    };
};

//Selecting a note
export const selectNote = (note) => {
    return (dispatch) => {
        dispatch({ type: SELECT_NOTE });
        return NotesService.getNoteById(note._id)
            .then((noteReceived) => {
                dispatch({ type: SELECT_NOTE_SUCCESS, payload: noteReceived });
            })
            .catch((error) => {
                dispatch({ type: SELECT_NOTE_FAILURE, payload: error });
            });
    };
};

//Opening section for showing details of a note selected
export const setShowNoteDetailsDrawer = (show, note) => {
    return {
        type: SET_SHOW_NOTE_DETAILS_DRAWER,
        payload: {
            show,
            note
        }
    };
};

//Capturing data changes from a form
export const formNoteChange = (newNote) => {
    return {
        type: NOTE_FORM_CHANGED,
        payload: newNote
    };
};

//Updating a note
export const setEditMode = (value) => {
    return {
        type: SET_EDIT_MODE,
        payload: value
    };
};

//Deleting a note
export const deleteNote = (note) => {
    return (dispatch) => {
        dispatch({ type: DELETE_NOTE });
        return NotesService.deleteNoteById(note._id)
            .then(() => {
                dispatch({ type: DELETE_NOTE_SUCCESS, payload: note });
            })
            .catch((error) => {
                dispatch({ type: DELETE_NOTE_FAILURE, payload: error });
            });
    };
};

//Opening section to confirm the deleting process of a note
export const setShowConfirmModal = (show, note) => {
    return {
        type: SET_SHOW_CONFIRM_MODAL,
        payload: {
            show,
            note
        }
    }
};