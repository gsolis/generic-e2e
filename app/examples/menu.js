import React, { PropTypes } from 'react';

const Menu = ({ children }) => {
    return (
        <ul className="option-menu">
            {children}
        </ul>
    );
}

Menu.propTypes = {
   children: PropTypes.any 
};

export default Menu;