import React from 'react';
import Menu from './menu';
import './css-review.scss';

class CssReview extends React.Component {

    constructor() {
        super();

        this.state = {
            showMenu: false,
            option1: false,
            option2: false,
            option3: false
        }
    }

    toggle = () => {
        this.setState({
            showMenu: !this.state.showMenu
        });
    }

    dropMenu = (e) => {
        const option = e.target.getAttribute('data-option'), // option1, option2, option3
            hiddenOptions = { option1: false, option2: false, option3: false };
        this.setState({ ...hiddenOptions, [option]: !this.state[option] });
    }

    render() {
        return (
            <div className="css-review-container">
                <div className="main-menu">
                    <label className="menu-toggle" onClick={this.toggle}>
                        J Luis <i className=""></i>
                    </label>
                    <div className={`menu ${this.state.showMenu ? 'menu-visible' : ''}`} >
                        <div className="menu-item">
                            <i className=""></i> Profile
                        </div>
                        <div className="menu-item">
                            <i className=""></i> Settings
                        </div>
                        <div className="menu-item">
                            <i className=""></i> Logout
                        </div>
                    </div>
                </div>

                <div className="options-container">
                    <div className="option" data-option="option1" onClick={this.dropMenu}>
                        Option 1
                        {this.state.option1 &&
                            <Menu>
                                <li>Option 1 - Menu item 1</li>
                                <li>Option 1 - Menu item 2</li>
                            </Menu>}
                    </div>
                    <div className="option" data-option="option2" onClick={this.dropMenu}>
                        Option 2
                        {this.state.option2 &&
                            <Menu> 
                                <li>Option 2 - Menu item 1</li>
                                <li>Option 2 - Menu item 2</li>
                            </Menu>}
                    </div>
                    <div className="option" data-option="option3" onClick={this.dropMenu}>
                        Option 3
                        {this.state.option3 &&
                            <Menu>
                                <li>Option 3 - Menu item 1</li>
                                <li>Option 3 - Menu item 2</li>
                            </Menu>}
                    </div>
                </div>
            </div>
        );
    }
}

export default CssReview;