import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';

class ComponentLifeCicle extends React.Component {
    
    //(Initialization)

    //Get initial props
    // ORDER : 1 ONLY ONCE
    constructor(props) {
        super(props);
        console.log('Initialization: props ->', props);

        // get / set intial state
        this.state = {
            value: 'abc',
            flag: false
        };
        console.log('Initialization: initial state ->', this.state);

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.filtrar = this.filtrar.bind(this);

    }

    //Mounting
    // ORDER : 2 ONLY ONCE
    componentWillMount() {
        console.log('Mounting: componentWillMount');
        this.setState({
            value: 'Changed before the initial render'
        });
    }

    // ORDER : 4 ONLY ONCE
    componentDidMount() {
        console.log('Mounting: componentDidMount');

        // setTimeout(() => {
        //     ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
        // }, 5000);
    }

    //Updating
    componentWillReceiveProps(nextProps) {
        console.log('Updating: componentWillReceiveProps', nextProps);
    }

    shouldComponentUpdate() {
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Updating: componentWillUpdate', nextProps, nextState);
        return false;
    }

    componentDidUpdate(previousProps, previousState) {
        console.log('Updating: componentDidUpdate', previousProps, previousState);
    }

    //Unmounting
    componentWillUnmount() {
        console.log('Unmounting: componentWillUnmount');
    }


    //Custom events
    handleClick() {
        this.setState({
            value: 'cba',
            flag: !this.state.flag
        });
    }

    handleChange(e) {
        console.log(e.target.value);
        this.setState({
            value: e.target.value
        });
    }

    filtrar(value) {
        if(value != 3) {
            return true;
        }else {
            return false;
        }
    }

    // ORDER : 3
    render() {
        console.log('RENDER');
        /*var myArray = [1, 2, 3, 4, 5, 6, 7];
        var newArray = myArray.filter(this.filtrar);
        console.log(newArray);
        console.log(myArray);*/


        return (
            <div className="app">
                <h1>{this.state.value}</h1>
                {this.state.flag &&
                    <input
                        type="text"
                        name="input1"
                        value={this.state.value}
                        onChange={this.handleChange} />}
                <button onClick={this.handleClick}>Btn 1</button>
            </div>
        );
    }

}

export default ComponentLifeCicle;