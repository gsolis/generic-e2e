import React from 'react';
//Importing Store created in an external file
import Store from './store';

class IncDec extends React.Component {

    constructor(props) {
        super(props);
        //Local state
        this.state = {
            value: undefined
        };

        //Subscribing to the Store passing a callback that is executed whenever the Store is updated
        Store.subscribe(this.handleStoreChange);
    }

    handleStoreChange = () => {
        this.setState(Store.getState());
    };

    handleInc = () => {
        const action = { type: 'INCREMENT'};
        Store.dispatch(action);
    }

    handleDec = () => {
        const action = { type: 'DECREMENT'};
        Store.dispatch(action);
    }

    render() {
        return (
            <div>
                <label>{this.state.value}</label><br />
                <label>{this.state.value2}</label>
                <button onClick={this.handleInc}>INC</button>
                <button onClick={this.handleDec}>DEC</button>
            </div>
        );
    }

}

export default IncDec;