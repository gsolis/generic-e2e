//Method for creating the Store imported from Redux
import { createStore } from 'redux';

//Object containing the initial state is created
const INITIAL_STATE = {
    value: 0,
    value2: 10,
    value3: 20,
    value4: 40
};

//The reducer responsible for handleling actions is created, it receives 2 parameters(state and action), returning a new state
const reducer = (state = INITIAL_STATE, action) => {
    let newState;
    switch (action.type) {
        case 'INCREMENT':
            //newState = { value: state.value + 1 };
            newState = {...state, value: state.value + 1, value2: state.value2 + 1}
        break;
        case 'DECREMENT':
            //newState = { value: state.value - 1 };
            newState = {...state, value: state.value - 1, value2: state.value2 - 1}
        break;
        default:
            newState = state;
    }
    return newState;
};

//Store is exported so our React component can imported and subscribe to it
export default createStore(reducer);