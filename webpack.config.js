var path = require('path'),
    webpack = require('webpack'),

config = {
    context: path.join(__dirname, 'app'),
    entry: './index.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['react']
                },
                exclude: /node_modules/
            },
            { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader', exclude: /node_modules/ },
            {
                test: /\.(png|jpg|ttf|eot|woff|woff2|svg)([\?]?.*)$/,
                loader: 'url-loader?limit=10000',
                exclude: /node_modules/
            }
        ]
    },
    devServer: {
        contentBase: './public'
    },
    devtool: 'source-map'
};

module.exports = config;