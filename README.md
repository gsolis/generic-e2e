# Generic E2E

## Notes Manager

### UI Views

* CREATE notes
* LIST notes
* VIEW single note
* EDIT/UPDATE note
* DELETE note

### Backend API

* GET /notes
* GET /notes/:id
* POST /notes
* PUT /notes/:id
* DELETE /notes/:id

### Note Model

{
    id: string,
    title: string,
    description: string,
    status: string,
    date: string
}

